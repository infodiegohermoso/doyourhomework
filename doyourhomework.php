

<?php
   
    $pdo = new PDO("mysql:host=localhost;dbname=mydb;charset=utf8","diego","Alumno1819"); // Conexión con la base de datos
	// Inserto los datos requeridos en la base de datos
    if(isset($_POST['submit']) ){
	$fechaHoy = date('Y-m-d');
	$fecha = date('Y-m-d');
	$fechaLimite = $_POST['fechaLimite'];
        $name = $_POST['name'];
        $user = $_POST['user'];
        $sth = $pdo->prepare("INSERT INTO todos (name,user,fecha,fechaLimite) VALUES (:name,:user,:fecha,:fechaLimite)");
        $sth->bindValue(':name', $name,PDO::PARAM_STR);
 	$sth->bindValue(':user',$user,PDO::PARAM_STR);
	$sth->bindValue(':fecha',$fecha,PDO::PARAM_STR);
	$sth->bindValue(':fechaLimite',$fechaLimite,PDO::PARAM_STR);
        $sth->execute();

    }elseif(isset($_POST['delete'])){ //  Eliminacion por ID de la tarea seleccionada
        $id = $_POST['id'];
        $sth = $pdo->prepare("delete from todos where id = :id");
        $sth->bindValue(':id', $id, PDO::PARAM_INT);
        $sth->execute();
    }
?>

<!DOCTYPE HTML>
<html lang="es">
<head>
    <title>DoYourHomeWork</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="CSS\normalize.css">
    <link rel="stylesheet" href="CSS\milligram.min.css">
<meta http-equiv="refresh" content="500"> 
</head>

<body class="container">
    <h1>DoYourHomeWork</h1>
    <form method="post" action="">
	<h3> Introduzca la tarea </h3>
        <input type="text" name="name" value="">
	<h3> Introduzca su nombre </h3>
        <input type="text" name="user" value="">
	<h3> Introduzca fecha finalización </h3>
	<input type="date" name="fechaLimite">
        <input type="submit" name="submit" value="Añadir">
    </form>
    <h2>Tabla</h2>
    <table class="table table-striped">
        <therad><th>Tareas</th><th></th></therad>
        <tbody>
<?php
    $sth = $pdo->prepare("SELECT * FROM todos ORDER BY id DESC"); // Preparacion por ID para seleccionar las tareas y recorrerlas
    $sth->execute();
    
    foreach($sth as $row) { // Recorro la base datos y almaceno en ARRAY
?>
            <tr>
                <td><b>Tarea:</b> <?=htmlspecialchars($row['name'])?><b> Creado Por: </b> <?=htmlspecialchars($row['user'])?> </br> <b> Fecha Creación: </b> <?=htmlspecialchars($row['fecha']) ?> <b> Fecha Limite: </b><?=htmlspecialchars($row['fechaLimite'])?> </td>
   
                <td>
                    <form method="POST">
                        <button type="submit" name="delete">Eliminar</button>
                        <input type="hidden" name="id" value="<?= $row['id'] ?>">
                        <input type="hidden" name="Eliminar" value="true">
                    </form>
                </td>
            </tr>
<?php
if($fechaHoy == $row['fechaLimite']) // Compruebo la fecha limite y si concuerda elimino de la base de datos
{
	$sth = $pdo -> prepare("DELETE FROM todos WHERE fecha = fechaLimite");
	$sth->execute();
}
    }
?>
        </tbody>
    </table>
</body>
</html>
